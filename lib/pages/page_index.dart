import 'package:flutter/material.dart';
import 'dart:math';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});



  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  bool _isStart = false;
  String inputText = '';
  List<String> hair = ['hair1', 'hair2', 'hair3', 'hair4', 'hair5', 'hair6'];
  List<String> face = ['face1', 'face2', 'face3', 'face4', 'face5', 'face6'];
  List<String> eyebrow = ['eyebrow1', 'eyebrow2', 'eyebrow3', 'eyebrow4', 'eyebrow5', 'eyebrow6'];
  List<String> eye = ['eye1', 'eye2', 'eye3', 'eye4', 'eye5', 'eye6'];
  List<String> nose = ['nose1', 'nose2', 'nose3', 'nose4', 'nose5', 'nose6'];
  List<String> mouth = ['mouth1', 'mouth2', 'mouth3', 'mouth4', 'mouth5', 'mouth6'];

  void _startGame() {
    setState(() {
      _isStart = true;
    });
  }

  @override
  void initState(){
    super.initState();
    setState(() {
        _isStart = true;
        hair.shuffle();
        face.shuffle();
        eyebrow.shuffle();
        eye.shuffle();
        nose.shuffle();
        mouth.shuffle();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('친구 얼굴 그리기'),
        centerTitle: true,
        backgroundColor: Colors.lightGreenAccent,
      ),
      body: Container(
        width: 500,
        height: 1000,
        color: Colors.amberAccent,
        child: _buildBody(context),
        ),
    );
  }
  
  Widget _buildBody(BuildContext context) {
    if (_isStart) {
      return SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      width: 400,
                      height: 400,
                      color: Colors.blueGrey,
                    ),
                    Container(
                      width: 250,
                      height: 250,
                      color: Colors.cyan,
                    ),
                    Container(
                      width: 125,
                      height: 125,
                      color: Colors.lightGreen,
                    ),
                    // Container(
                    //     width: 500,
                    //     height: 500,
                    // ),
                    // Container(
                    //     width: 500,
                    //     height: 500,
                    // ),
                    // Container(
                    //     width: 500,
                    //     height: 500,
                    // ),
                  ],
                ),
              ),
              Text('$inputText 얼굴 입니다'),
              Container(
                child: OutlinedButton(
                  onPressed: () {
                    setState(() {
                      _isStart = false;
                    });
                  },
                  child: const Text('종료'),
                ),
              )
            ],
          ),
      );
    } else {
      return SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Container(
                  margin: EdgeInsets.all(100),
                  child: TextField(
                    onChanged: (text){
                      setState(() {
                        inputText = text;
                      });
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: '이름을 입력 하세요',
                    ),
                  )
              ),
              OutlinedButton(
                onPressed: () {
                  _startGame();
                },
                child: const Text('시작'),
              ),
            ],
          ),
        ),
      );
    }
  }
}