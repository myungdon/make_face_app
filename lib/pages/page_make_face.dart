import 'package:flutter/material.dart';

class PageMakeFace extends StatefulWidget {
  const PageMakeFace({
    super.key,
    required this.friendName
  });

  final String friendName;

  @override
  State<PageMakeFace> createState() => _PageMakeFaceState();
}


class _PageMakeFaceState extends State<PageMakeFace> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('결과'),
      ),
      body: Text(widget.friendName),
    );
  }
}
